DIRECTORY STRUCTURE
-------------------

```
documents/
    about.txt              краткое описание
    db_table.txt           команда для создания таблиц в базе данных
    request_doc.txt        документация по запросам

examples/
    config.php             конфигурация
    functions.php          вспомогательные функции
    test_add.php           тест добавления сообщения
    test_archive.php       тест вывода архива
    test_delete.php        тест удаления сообщения
    test_edit.php          тест редактировани сообщения
    test_files_upload.php  тест загрузки файлов
    test_file_get.php      тест чтения файла

chat.go                    функции обработки команд чата
files.go                   функции работы с файлами
dbase.go                   функция подключения к базе данных
dbconnect.cfg              концигурация базы данных
document.go                структуры
functions.go               вспомогательные функции
main.go                    листенер, обработчик http-запросов, роутер
```
