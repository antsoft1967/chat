package main

import (
	"github.com/lib/pq"
	"database/sql"
	"strings"
	"time"
	"fmt"
)

// Новое сообщение в чат
func chat_add(db *sql.DB, community, userId string, chatData ChatRequestAdd, userLastTime, timenow time.Time) (map[string]interface{}, bool) { 
	
	var out map[string]interface{};
	
	// Если есть массив получателей, то нормализуем его (дописываем пользователя в массив)
	if len(chatData.Receivers) > 0 {
		chatData.Receivers = receiversPrepare(userId, chatData.Receivers);
	}
	
	// Сохраняем сообщение в индивидуальный или групповой чат
	result, err := db.Exec("INSERT INTO chat (community, date, sender, message, receivers) values ($1, $2, $3, $4, $5)", community, timenow, userId, chatData.Message, pq.Array(chatData.Receivers));
	result_count, result_err := result.RowsAffected();
	if err != nil || result_err != nil || result_count == 0 {
		return out, false;
	}
	
	// Получаем ленту сообщений
	zerotime := time.Time{};
	messages := getMessages(db, community, userId, chatData.Receivers, userLastTime, zerotime, 0, 0);
	
	// Возвращаем результат
	out = map[string]interface{}{"messages": messages};
	return out, true;
}

// Редактирование сообщения
func chat_edit(db *sql.DB, messageId, userId string, chatData ChatRequestEdit, timenow time.Time) (map[string]interface{}, bool) { 
	
	var out map[string]interface{};
	
	// Если есть текст сообщения, то добавляем его в чат
	if chatData.Message != "" {
		
		// Обновляем текст сообщения и время модификации
		result, err := db.Exec("UPDATE chat SET message = $1, modify = $2 WHERE id = $3 AND sender = $4", chatData.Message, timenow, messageId, userId);
		result_count, result_err := result.RowsAffected();
		if err != nil || result_err != nil || result_count == 0 {
			return out, false;
		}
	}
	
	out = map[string]interface{}{"modify": timenow};
	return out, true;
}

// Удаление сообщения
func chat_delete(db *sql.DB, messageId, userId string, chatData ChatRequestDelete, timenow time.Time) (bool) { 
	
	var (
		nullString sql.NullString;
		deleteTarget []sql.NullString;
	)
	
	row := db.QueryRow("SELECT * FROM chat WHERE id = $1 AND sender = $2", messageId, userId);
	
	p := ChatScanModel{};
	err := row.Scan(&p.id, &p.community, &p.date, &p.modify, &p.sender, &p.message, pq.Array(&p.receivers), pq.Array(&p.deleted));
	if err != nil {
		return false;
	}
	
	// Нормализуем флаг удаления
	chatData.Target = strings.ToLower(chatData.Target);
	
	// Формируем массив целей для удаления
	if chatData.Target == "me" { // Если удаление только для запросившего, то добавляем его id в массив
		
		nullString.String = userId;
		nullString.Valid = true;
		p.deleted = append(p.deleted, nullString);
		deleteTarget = p.deleted;
		
	} else if chatData.Target == "all" { // Если удаление для всех, то массив удалённых = это массив получателей
		
		// Если это сообщение общего чата
		if len(p.receivers) == 0 {
			nullString.String = chatData.Target;
			nullString.Valid = true;
			deleteTarget = append(deleteTarget, nullString);
		} else {
			deleteTarget = p.receivers;
		}
	} else {
		return false;
	}
	
	// Обновляем поле deleted
	result, err := db.Exec("UPDATE chat SET deleted = $1, modify = $2 WHERE id = $3 AND sender = $4", pq.Array(deleteTarget), timenow, messageId, userId);
	result_count, result_err := result.RowsAffected();
	if err != nil || result_err != nil || result_count == 0 {
		return false;
	}
	
	return true;
}

// Архив сообщений чата
func chat_archive(db *sql.DB, community, userId string, chatData ChatRequestArchive) (map[string]interface{}, bool) {
	
	var (
		count, skip int;
		out map[string]interface{};
		datebegin, dateend time.Time;
	)
	
	// Количество сообщений в ответе
	if count = chatData.Count; count == 0 {
		count = 10;
	}
	
	// Пропустить количество сообщений
	if skip = chatData.Skip; skip == 0 {
		skip = 0;
	}
	
	// Если в запросе присутствует время начала архива
	if chatData.DateBegin > 0 {
		datebegin = time.Unix(chatData.DateBegin, 0);
	} else {
		datebegin = time.Time{};
	}
	
	// Если в запросе присутствует время окончания архива
	if chatData.DateEnd > 0 {
		dateend = time.Unix(chatData.DateEnd, 0);
	} else {
		dateend = time.Time{};
	}
	
	// Если есть массив получателей, то нормализуем его (дописываем пользователя в массив)
	if len(chatData.Receivers) > 0 {
		chatData.Receivers = receiversPrepare(userId, chatData.Receivers);
	}
	
	// Получаем ленту сообщений
	messages := getMessages(db, community, userId, chatData.Receivers, datebegin, dateend, count, skip);

	// Возвращаем результат
	out = map[string]interface{}{"messages": messages};
	return out, true;
}

// Получить ленту сообщений
func getMessages(db *sql.DB, community, userId string, receivers []string, datebegin, dateend time.Time, count, skip int) ([]ChatListModel) {
	
	var (
		rows *sql.Rows;
		messages []ChatListModel;
		countStr string;
	)
	
	if count == 0 {
		//countStr = "ALL";
		countStr = "100";
	} else {
		countStr = fmt.Sprint(count);
	}
	
	// Стандартное начало запроса
	requestBeginStr := "SELECT * FROM chat WHERE community = $1 AND ((NOT ($2 = ANY (coalesce(deleted, array[]::text[]))) AND NOT ('all' = ANY (coalesce(deleted, array[]::text[])))) OR deleted IS NULL) ";
	
	// Если есть начальное время архива
	if datebegin.IsZero() == false {
		requestBeginStr = requestBeginStr + "AND date >= to_timestamp(" + fmt.Sprint(datebegin.Unix()) + ") ";
	}
	
	// Если есть конечное время архива
	if dateend.IsZero() == false {
		requestBeginStr = requestBeginStr + "AND date <= to_timestamp(" + fmt.Sprint(dateend.Unix()) + ") ";
	}
	
	// Формируем запрос
	if len(receivers) > 0 { // Если это индивидуальный или групповой чат
		
		rows, err = db.Query(requestBeginStr + "AND receivers = $3 ORDER BY date DESC LIMIT $4 OFFSET $5", community, userId, pq.Array(receivers), countStr, skip);
		if err != nil {
			fmt.Println(err);
			return messages;
		}
		
	} else { // Если это общий чат
		
		rows, err = db.Query(requestBeginStr + "ORDER BY date DESC LIMIT $3 OFFSET $4", community, userId, countStr, skip);
		if err != nil {
			fmt.Println(err);
			return messages;
		}
	}
	
	// Сканируем полученный результат, парсим и формируем массив ответа
	for rows.Next() {
		p := ChatScanModel{}
		err := rows.Scan(&p.id, &p.community, &p.date, &p.modify, &p.sender, &p.message, pq.Array(&p.receivers), pq.Array(&p.deleted), pq.Array(&p.files));
		if err != nil{
			fmt.Println(err);
			continue;
		}
		
		// Карта для вывода файлов
		var fileInfoArray []FileBaseModel;
		
		// Если к сообщению прикреплены файлы
		if len(p.files) > 0 {
			rowsfiles, errfiles := db.Query("SELECT * FROM files WHERE id = ANY ($1)", pq.Array(p.files));
			if errfiles == nil {
				for rowsfiles.Next() {
					p := FileScanModel{};
					err := rowsfiles.Scan(&p.id, &p.title, &p.extension, &p.mime_type, &p.size);
					if err == nil{
						ff := FileBaseModel{Id: p.id, Title: p.title, Ext: p.extension, Mime: p.mime_type, Size: p.size};
						fileInfoArray = append(fileInfoArray, ff);
					}
				}
			}
		}
		
		// Добавляем сообщение к результатам вывода
		pp := ChatListModel{Id: p.id, Date: p.date, Userid: p.sender, Message: p.message, Files: fileInfoArray};
		messages = append(messages, pp);
	}
	
	return messages;
}