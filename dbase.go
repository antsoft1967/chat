package main

import (
	"database/sql"
	"fmt"
)

// Коннектимся к базе данных
func dbOpen() (*sql.DB, bool) {
	
	if (dbpass == "") {
		connStr = fmt.Sprintf("host=%s port=%d user=%s dbname=%s sslmode=disable", dbhost, dbport, dbuser, dbname);
	} else {
		connStr = fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", dbhost, dbport, dbuser, dbpass, dbname);	
	}
	
	db, err := sql.Open("postgres", connStr);
	if err != nil {
		return db, false;
	}
	
	return db, true;
}
