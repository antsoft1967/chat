package main

import (
	"database/sql"
	"time"
)

// Формат файла конфигурации
type ConnectModel struct{
	DbHost	string `json:"dbhost"`
	DbPort	int `json:"dbport"`
	DbName	string `json:"dbname"`
	DbUser	string `json:"dbuser"`
	DbPass	string `json:"dbpassword"`
	ApiPort int `json:"apiport"`
	DirTmp string `json:"dirtmp"`
}

// Модель чата для сканирования
type ChatScanModel struct{
	id		int64
	community	string
	date		string
	modify		sql.NullString
	sender		string
	message		string
	files		[]string
	receivers	[]sql.NullString
	deleted		[]sql.NullString
}

// Модель файлов для сканирования
type FileScanModel struct{
	id		string
	title		string
	extension	string
	mime_type	string
	size		int
}

// Базовая модель файлов
type FileBaseModel struct{
	Id		string `json:"id"`
	Title		string `json:"title"`
	Ext		string `json:"extension"`
	Mime		string `json:"mime_type"`
	Size		int `json:"size"`
}

// Модель чата для удаления
type ChatDeleteModel struct{
	receivers	[]sql.NullString
	deleted		[]sql.NullString
}

// Модель для возврата списка сообщений
type ChatListModel struct {
	Id		int64 `json:"id"`
	Date		string `json:"date"`
	Userid		string `json:"sender"`
	Message		string `json:"message"`
	Files		[]FileBaseModel `json:"files"`
}

// Модель запроса на добавление сообщения
type ChatRequestAdd struct {
	Message		string `json:"message"`
	Receivers	[]string `json:"receivers"`
}

// Модель запроса на редактирование сообщения
type ChatRequestEdit struct {
	Message		string `json:"message"`
}

// Модель запроса на редактирование сообщения
type ChatRequestDelete struct {
	Target		string `json:"target"`
}

// Модель запроса на получение архива
type ChatRequestArchive struct {
	DateBegin	int64 `json:"datebegin"`
	DateEnd		int64 `json:"dateend"`
	Count		int `json:"count"`
	Skip		int `json:"skip"`
	Receivers	[]string `json:"receivers"`
}

// Модель чата для сканирования
type UserScanModel struct{
	id		string
	name		string
	last_auth	time.Time
	token		string
}

// Модель для возврата списка активных пользователей
type UserListModel struct {
	Userid		string	`json:"id"`
	UserName	string	`json:"name"`
}
