<?php

// Функция curl-запроса
function docurl($url, $token = '', $postdata = array(), $datatype = 'json') { 

	if ($datatype == 'json') {
		$header = array(
			'Accept: application/json',
			'Content-Type: application/json; charset=utf-8',
			$token,
			"Cache-Control", "no-cache"
		);
		
		$data = json_encode($postdata);
		
	} else if ($datatype == 'multipart') {
		$header = array(
			'Accept: multipart/form-data',
			'Content-Type: multipart/form-data',
			$token,
			"Cache-Control", "no-cache"
		);
		
		if (!isset($postdata['MAX_FILE_SIZE'])) {
			$postdata['MAX_FILE_SIZE'] = 30000;
		}
		
		$data = $postdata;
		
	} else {
		return false;
	}
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	
	$result = curl_exec($ch);
	curl_close($ch);
	
	if ($result == null) {
		return false;
	} else {
		return $result;
	}
}
?>