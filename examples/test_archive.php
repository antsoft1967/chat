<?php

// Тест вывода архива

include 'config.php';
include 'functions.php';

$url = 'http://'.$host.':'.$port.'/chat/'.$community.'/archive/';

$what = array(
	'datebegin' => time() - 86400 * 7, // Неделю назад
	'dateend' => time(),
	'count' => 3,
	'skip' => '0',
);

echo $result = docurl($url, $token, $what);
exit;

?>