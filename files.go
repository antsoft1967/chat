package main

import (
	"github.com/google/uuid"
	"github.com/lib/pq"
	"path/filepath"
	"database/sql"
	"net/http"
	//"strconv"
	"bufio"
	"fmt"
	"io"
	"os"
)

// Пакетное сохранение файлов сообщения на сервер
func files_upload(r *http.Request, db *sql.DB, messageId string) (bool) {
	
	var (
		fileNamesArray []uuid.UUID;
		fileInfoArray []FileBaseModel;
	)
	
	// Парсим перечень файлов из запроса
	mr, err := r.MultipartReader()
	if err != nil {
		return false;
	}
	
	// Создаём временную папку для хранения файлов, если её нет
	dir := makeDirTmp(dirtmp, "");
	
	// Читаем список файлов
	for {
		// Берём очредной элемент списка файлов
		part, err := mr.NextPart();
		
		if err == io.EOF {
			break;
		}
		
		// Устанавливаем новый читатель
		reader := bufio.NewReader(part);
		
		// Вычленяем только имя файла
		_, filename := filepath.Split(part.FileName());
		if filename == "" {
			continue;
		}
		
		// Генерируем id для имени файла и сохраняем его в карту имён
		fileid, _ := uuid.NewRandom();
		fileNamesArray = append(fileNamesArray, fileid);
		
		// Создаём в папке назначения пустой файл и открываем его для записи
		fileDestination, err := os.Create(dir + "/" + fileid.String());
		if err != nil {
			return false;
		}
		defer fileDestination.Close();
		
		// Создаём промежуточный буфер
		buffer := make([]byte, 4096);
		
		// Устанавливаем новый писатель
		writer := bufio.NewWriter(fileDestination);
		
		// Переменная для точного имени файла
		var filesize int;
		
		// Читаем файл по кускам
		for {
			// Читаем в буфер памяти данные из пришедшего файла
			count, err := reader.Read(buffer);
			if (err != nil && err != io.EOF) || count == 0 {
				break;
			}
			
			// Пишем данные из буфера в файл назначения
			_, err = writer.Write(buffer[:count]);
			if err != nil {
				break;
			}
			writer.Flush();
			
			filesize = filesize + count;
		}
		
		// Формируем массив данных о файле
		ext := filepath.Ext(filename);
		title := filename[0:len(filename) - len(ext)];
		ff := FileBaseModel{Id: fileid.String(), Title: title, Ext: ext, Mime: http.DetectContentType(buffer), Size: filesize};
		fileInfoArray = append(fileInfoArray, ff);
		
		//fmt.Println(part.Header.Get("Content-Type"));
		//fmt.Println(http.DetectContentType(buffer));
	}
	
	// Если массив имён файлов не пустой, то сохраняем информацию о файлах в базу
	if len(fileInfoArray) > 0 {
		
		// Формируем строку запроса для добавления имён файлов
		requestStr := "INSERT INTO files (id, title, extension, mime_type, size) VALUES ";
		for i :=0; i < len(fileInfoArray); i++ {
			requestStr = requestStr + "('" + fileInfoArray[i].Id + "', '" + fileInfoArray[i].Title + "', '" + fileInfoArray[i].Ext + "', '" + fileInfoArray[i].Mime + "', " + fmt.Sprint(fileInfoArray[i].Size) + "),";
		}
		requestStr = requestStr[:len(requestStr) - 1];
		
		// Сохраняем сведения о файлах в таблицу files
		result, err := db.Exec(requestStr);
		result_count, result_err := result.RowsAffected();
		if err != nil || result_err != nil || result_count == 0 {
			return false;
		}
		
		// Обновляем поле files в таблице chat
		result, err = db.Exec("UPDATE chat SET files = $1 WHERE id = $2", pq.Array(fileNamesArray), messageId);
		result_count, result_err = result.RowsAffected();
		if err != nil || result_err != nil || result_count == 0 {
			return false;
		}
	}
	
	return true;
}

// Получить файл с сервера
func file_get(w http.ResponseWriter, r *http.Request, db *sql.DB, fileId string) (bool) {
	
	// Создаём путь к папке
	dir := dirtmp;
	
	// Читаем имя файла по его номеру
	row := db.QueryRow("SELECT * FROM files WHERE id = $1", fileId);
	p := FileScanModel{};
	err := row.Scan(&p.id, &p.title, &p.extension, &p.mime_type, &p.size);
	if err != nil {
		return false;
	}
	
	// Открываем файл
	openFile, err := os.Open(dir + "/" + p.id);
	defer openFile.Close();
	if err != nil {
		return false;
	}
	
	// Буфер для заголовка файла
	FileHeader := make([]byte, 512);
	
	// Копируем заголовки файла в буфер
	openFile.Read(FileHeader);
	
	// Получаем тип контента файла
	FileContentType := http.DetectContentType(FileHeader);
	
	// Получаем размер файла и конвертируем его в строку
	FileStat, _ := openFile.Stat();
	//FileSize := strconv.FormatInt(FileStat.Size(), 10);
	FileSize := fmt.Sprint(FileStat.Size());
	
	// Отправляем на вывод заголовки
	w.Header().Set("Content-Disposition", "attachment; filename=" + p.title + p.extension);
	w.Header().Set("Content-Type", FileContentType);
	w.Header().Set("Content-Length", FileSize);
	
	// Отправляем файл на вывод
	openFile.Seek(0, 0);
	io.Copy(w, openFile);
	
	return true;
}