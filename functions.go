package main

import (
	"encoding/json"
	"path/filepath"
	"database/sql"
	"net/http"
	"reflect"
	"strings"
	"sort"
	"time"
	"fmt"
	"os"
	"io"
)

// Читаем конфигурацию сервера и базы данных
func readConfig() {
	
	var dataConnect ConnectModel;
	
	cfg := readFile("config.cfg");
	err := json.Unmarshal([]byte(cfg), &dataConnect);
	
	if err == nil {
		dbhost = dataConnect.DbHost;
		dbport = dataConnect.DbPort;
		dbname = dataConnect.DbName;
		dbuser = dataConnect.DbUser;
		dbpass = dataConnect.DbPass;
		apiport = dataConnect.ApiPort;
		dirtmp = dataConnect.DirTmp;
	} else {
		dbhost = "localhost";
		dbport = 5432;
		dbname = "db";
		dbuser = "postgres";
		dbpass = "";
		apiport = 9000;
		dirtmp = "/srv/www/go/chat_postgre/tmp/";
	}
}

// Идентифицируем пользователя
func userIdent(db *sql.DB, r *http.Request) (string, time.Time, time.Time, bool) {
	
	var (
		userId, userToken string;
		userLastTime time.Time;
	)
	
	// Фиксируем время запроса
	timenow := time.Now();
	
	// Извлекаем токен из заголовка запроса
	tokenGet := r.Header.Get("Authorization");
	tokenGet = strings.TrimSpace(tokenGet);
	tokenGet = strings.Replace(tokenGet, "  ", " ", -1);
	tokenArray := strings.Split(tokenGet, " ");
	if tokenArray[0] == "Bearer" {
		userToken = tokenArray[1];
	} else {
		fmt.Println(err);
		return userId, userLastTime, timenow, false;
	}
	
	// Идентифицируем пользователя по токену
	row := db.QueryRow("SELECT * FROM \"user_tmp\" WHERE token = $1", userToken);
	p := UserScanModel{};
	err = row.Scan(&p.id, &p.name, &p.last_auth, &p.token);
	if err != nil {
		fmt.Println(err);
		fmt.Println("-----");
		return userId, userLastTime, timenow, false;
	}
	
	userId = p.id; // ID пользователя
	
	//if p.last_auth.Valid {
	//	userLastTime = p.last_auth.String; // Последний сеанс
	//}
	userLastTime = p.last_auth; // Последний сеанс
	
	// Обновляем время сеанса
	result, err := db.Exec("UPDATE \"user_tmp\" SET last_auth = $1 WHERE id = $2", timenow, userId);
	result_count, result_err := result.RowsAffected();
	if err != nil || result_err != nil || result_count == 0 {
		return userId, userLastTime, timenow, false;
	}
	
	return userId, userLastTime, timenow, true;
}

// Нормализация массива получателей
func receiversPrepare(userId string, receivers []string) []string {
	
	// Если пользователя нет в массиве получателей, то добавляем его
	if ok, _ := inArray(userId, receivers); ok == false {
		receivers = append(receivers, userId);
	}
	
	// Сортируем массив
	sort.Strings(receivers);
	
	return receivers;
}

// Оформляем вывод результата
func returnResult(w http.ResponseWriter, verify bool, description string, addout map[string]interface{}) {
	
	out := map[string]interface{}{};
	
	if len(addout) > 0 {
		for k, v := range addout {
			out[k] = v;
		}
	}
	
	out["verify"] = verify;
	
	if (addout["description"] == nil) {
		out["description"] = description;
	} else {
		out["description"] = addout["description"];
	}
	
	b, _ := json.Marshal(out);
	
	w.Write(b);
	return;
}

// Поиск элемента в массиве
func inArray(val interface{}, array interface{}) (exists bool, index int) {
	
	exists = false;
	index = -1;
	
	switch reflect.TypeOf(array).Kind() {
		case reflect.Slice:
			s := reflect.ValueOf(array);
			
			for i := 0; i < s.Len(); i++ {
				if reflect.DeepEqual(val, s.Index(i).Interface()) == true {
					index = i;
					exists = true;
					return;
				}
			}
	}
	return;
}

// Читаем файл
func readFile(path string) (string) {
	
	var data string;
	
	if _,fault := os.Stat(path); fault != nil {
		if os.IsNotExist(fault) {
			return data;
		}
	}
	file,fault := os.Open(path);
	if fault != nil {
		return data;
	}
	defer file.Close();
	var buf []byte = make([]byte, 1024);
	
	for {
		count,fault := file.Read(buf);
		if count > 0 {
			data = string(buf[:count]);
		}
		if fault != nil {
			if fault != io.EOF {
				return data;
			}
			break;
		}
	}
	return data;
}

// Создать папку если не существует
func makeDirTmp(dir, subdir string) (string) {
	dir = dir + "/" + subdir;
	if fileExists(dir + "/" + subdir) == false {
		_ = os.Mkdir(dir, 0755);
	}
	return dir;
}

// Проверка наличия файла или папки
func fileExists(filename string) (bool) {
	_, err := os.Stat(filename); 
	ret := os.IsNotExist(err);
	if ret == false {
		return true;
	} else {
		return false;
	}
}

// Изменить имя файла (увеличить счётчик)
func makeFileName(filename string) (string) {
	
	ext := filepath.Ext(filename);
	filename = filename[0:len(filename) - len(ext)] + "_1" + ext;
	
	return filename;
}