package main
import (
	"github.com/gorilla/mux"
	"encoding/json"
	"net/http"
	"time"
	"fmt"
	"log"
)

var (
	str  string;
	err  error;
	
	dbhost, dbname, dbuser, dbpass, connStr, dirtmp string;
	dbport, apiport int;
	
	// Задаём значения сообщений об ошибках и об успешных результатах
	resultMessage map[bool]string = map[bool]string{true: "Ok", false: "Error"};
)

// Роутер пустого запроса
func Home(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`{"verify":false,"description":"` + resultMessage[false] + `"}`))
}

// Роутер чата
func ChatRouter(w http.ResponseWriter, r *http.Request) {
	
	var (
		verify bool;
		userId string;
		timenow, userLastTime time.Time;
		out map[string]interface{}
	)
	
	// Коннектимся к базе данных
	db, verify := dbOpen();
	defer db.Close();
	if verify == false {
		returnResult(w, verify, resultMessage[verify], out);
		return;
	}
	
	// Идентифицируем пользователя
	if userId, userLastTime, timenow, verify = userIdent(db, r); verify == false {
		returnResult(w, false, "Wrong users token", out);
		return;	
	}
	
	// Парсим строку запроса
	vars := mux.Vars(r);
	
	switch vars["command"] {
		
		case "add": // Отправить сообщение и получить новые
			
			// Парсим тело запроса и извлекаем сообщение и получателей
			var chatData ChatRequestAdd;
			json.NewDecoder(r.Body).Decode(&chatData);
			
			// Добавляем сообщение в чат и возвращаем список новых
			out, verify = chat_add(db, vars["id"], userId, chatData, userLastTime, timenow);
			returnResult(w, verify, resultMessage[verify], out);
			//http.Error(w, resultMessage[verify], 400);
			return;
			
		
		case "archive": // Получить архив сообщений
			
			// Парсим тело запроса и извлекаем сообщение и получателей
			var chatData ChatRequestArchive;
			json.NewDecoder(r.Body).Decode(&chatData);
			
			// Возвращаем архив сообщений
			out, verify = chat_archive(db, vars["id"], userId, chatData);
			returnResult(w, verify, resultMessage[verify], out);
			return;
			
		case "edit": // Отправить сообщение и получить новые
			
			// Парсим тело запроса и извлекаем текст сообщения
			var chatData ChatRequestEdit;
			json.NewDecoder(r.Body).Decode(&chatData);
			
			// Редактируем сообщение
			out, verify = chat_edit(db, vars["id"], userId, chatData, timenow);
			returnResult(w, verify, resultMessage[verify], out);
			return;
		
		case "delete": // Получить архив сообщений
			
			// Парсим тело запроса и извлекаем сообщение и получателей
			var chatData ChatRequestDelete;
			json.NewDecoder(r.Body).Decode(&chatData);
			
			// Возвращаем архив сообщений
			verify = chat_delete(db, vars["id"], userId, chatData, timenow);
			returnResult(w, verify, resultMessage[verify], out);
			return;
			
		default:
			returnResult(w, false, "Wrong command", out);
			return;
	}
}

// Роутер файлов
func FileRouter(w http.ResponseWriter, r *http.Request) {
	
	var (
		verify bool;
		out map[string]interface{}
	)
	
	// Коннектимся к базе данных
	db, verify := dbOpen();
	defer db.Close();
	if verify == false {
		returnResult(w, verify, resultMessage[verify], out);
		return;
	}
	
	// Идентифицируем пользователя
	if _, _, _, verify = userIdent(db, r); verify == false {
		returnResult(w, false, "Wrong users token", out);
		return;
	}
	
	// Парсим строку запроса
	vars := mux.Vars(r);
	
	switch vars["command"] {
		
		case "upload": // Загрузить файлы на сервер
			
			verify = files_upload(r, db, vars["id"]);
			returnResult(w, verify, resultMessage[verify], out);
			return;
		
		case "get": // Получить файл с сервера
			
			verify = file_get(w, r, db, vars["id"]);
			returnResult(w, verify, resultMessage[verify], out);
			return;
			
		default:
			returnResult(w, false, "Wrong command", out);
			return;
	}
}

func main() {
	
	// Читаем конфигурацию
	readConfig();
	
	// Слушаем порт
	r := mux.NewRouter();
	r.HandleFunc("/", Home); // Роутер пустого запроса
	r.HandleFunc("/chat/{id}/{command}/", ChatRouter); // Роутер чата
	r.HandleFunc("/file/{id}/{command}/", FileRouter); // Роутер чата
	http.Handle("/", r);
	
	fmt.Println("Server is listening port number " + fmt.Sprint(apiport) + "...")
	fmt.Println(" ");
	
	err = http.ListenAndServe(":" + fmt.Sprint(apiport), nil); // Слушаем порт
	if err != nil {
		log.Fatal("Listen error: ", err);
	}
}